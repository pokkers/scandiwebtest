<?php
    include 'dbc.php';
    include 'products.php';
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- main css -->
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

</head>
<body>
    <!-- NAVIGATION -->
    <nav class="navbar navbar-expand navbar-dark bg-dark">
        <a class="navbar-brand" href="/">Products</a>
        <a class="navbar-brand" href="/add.php">Add products</a>
    </nav>
    <!-- end of NAVIGATION -->
    <!-- Product "add" section-->
    <div class="container">
        <form class="needs-validation" method="POST">
            <div class="row">
                <div class="col-12 mt-3">
                    <label for="sku">SKU</label>
                    <small>*Must be unique value</small>
                    <input type="text" class="form-control" id="sku" name="sku" >
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <label for="product">Product name</label>
                    <input type="text" class="form-control" id="productName" name="productName" >
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <label for="price">Product price</label>
                    <input type="text" class="form-control" id="productPrice" name="productPrice" >
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <label for="type">Product type</label>
                    <select class="form-control" id="productType" name="productType" >
                        <option value="-1">Select product type</option>
                        <option value="1" name="book">Book</option>
                        <option value="2" name="cd">CD</option>
                        <option value="3" name="furniture">Furniture</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <label for="atribute">Atribute</label><br>
                    <small class="items alert-danger item-1">Please select product type</small>
                    <small class="items alert-danger item1">Please provide weight in kg</small>
                    <small class="items alert-danger item2">Please provide amount of memory in Mb</small>
                    <small class="items alert-danger item3">Please provide dimensions in HxWxL format</small>
                    <span class="form-inline items item1 ">
                        <div class="col-12 col-lg-6">WEIGHT: </div><input type="text" class="form-control col-8 form-inline" id="productAtribute" name="weight" >&nbsp;
                        <span class="col-2">kg</span>
                        <div class="row col-12">
                            <div class="mt-3">
                                <button class="col-12 btn btn-success btn-lg btn-block" type="submit" name="bookadd">Add product</button>
                            </div>
                        </div>
                        <?php if(isset($_POST['bookadd'])) {
                            $q = new Book();
                            $w = $q->insert();
                        }elseif(isset($_POST['cdadd'])) {
                            $d = new Cd();
                            $e = $d->insert();
                         }elseif(isset($_POST['addFurniture'])) {
                            $f = new Furniture();
                            $k = $f->insert();
                         } ?>
                    </span>
                    <span class="form-inline items item2 ">
                        <div class="col-12 col-lg-6">SIZE: </div><input type="text" class="form-control col-8 form-inline" id="productAtribute" name="size" >&nbsp;
                        <span>Mb</span>
                        <div class="row col-12">
                            <div class="mt-3">
                                <button class="col-12 btn btn-success btn-lg btn-block" type="submit" name="cdadd">Add product</button>
                            </div>
                        </div>
                    </span>
                    <span class="form-inline items item3 ">
                        <div class="col-12 col-lg-6">HEIGHT: </div><input type="text" class="form-control col-8 form-inline" id="productAtribute" name="height">&nbsp;
                        <span>Cm</span>
                        <div class="col-12 col-lg-6">WIDTH: </div><input type="text" class="form-control col-8 form-inline" id="productAtribute" name="width" >&nbsp;
                        <span>Cm</span>
                        <div class="col-12 col-lg-6">LENGHT: </div><input type="text" class="form-control col-8 form-inline" id="productAtribute" name="lenght">&nbsp;
                        <span>Cm</span>
                        <div class="row col-12">
                            <div class="mt-3">
                                <button class="col-12 btn btn-success btn-lg btn-block" type="submit" name="addFurniture">Add product</button>
                            </div>
                        </div>
                    </span>
                </div>
            </div>
        </form>
    </div>

    <!-- end of product "add" section-->
    <!-- jQuery js -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <!-- custom js -->
    <script src="/assets/main.js"></script>
    <!-- bootsrap jQuery js -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <!-- bootstrap js -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <!-- bootsrap popper js -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
</body>
</html>
