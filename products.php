<?php 

abstract class Products extends Dbc {
    
    abstract function getAttributeType();
    abstract function setAttributeType();
//---Getting all products from database---//
    
    public function getProducts() {
        $attr = $this->setAttributeType();
        $sql = "SELECT * FROM product WHERE Type = :a ";
        $stmt = $this->connect()->prepare($sql); 
        $stmt->bindParam('a', $attr);
        $stmt->execute();

        while($row = $stmt->fetch()){
            $data[] = $row;
        }
        return $data;
    }

    public function insert() {

        $sku = $_POST['sku'];
        $product = $_POST['productName'];
        $price = $_POST['productPrice'];
        $type = $this->setAttributeType();
        $attr =$this->getAttributeType();
        //Check if SKU number allready exists
        $skuCheck = "SELECT COUNT(*) AS num FROM product WHERE SKU = :s";
        $stmtCheck = $this->connect()->prepare($skuCheck);
        $stmtCheck->bindParam('s', $sku);
        $stmtCheck->execute();
        $row = $stmtCheck->fetch();
        if($row['num'] > 0){
            echo "<script>alert('SKU must be unique!');</script>";
        }else {
        //SKU unique - insert into DB
            $sql = "INSERT INTO product (SKU, Name, Price, Type, Atribute) VALUES(:a, :b, :c, :d, :e)";
            $stmt = $this->connect()->prepare($sql);
            $stmt->bindParam('a', $sku);
            $stmt->bindParam('b', $product);
            $stmt->bindParam('c', $price);
            $stmt->bindParam('d', $type);
            $stmt->bindParam('e', $attr);
            if($stmt->execute()){
                echo "<script>alert('Record created successfully!');</script>";
                echo "<script>window.location.href = 'add.php';</script>";
                    
            } else {
                    echo "<script>alert('All fields are mandatory for submision');</script>";
            }
        }
    }

}

class Furniture extends Products {

    public function getAttributeType(){
        if(!empty($_POST['sku']) && !empty($_POST['productName']) && !empty($_POST['productPrice']) && !empty($_POST['productType']) && !empty($_POST['height']) && !empty($_POST['width']) && !empty($_POST['lenght'])) {
            $attr = $_POST['height']."x".$_POST['width']."x".$_POST['lenght']." CM";
            return $attr; 
        }
    }
    public function setAttributeType(){
        return "furniture";
    }
}

class Cd extends Products {

    public function getAttributeType(){
        if(!empty($_POST['sku']) && !empty($_POST['productName']) && !empty($_POST['productPrice']) && !empty($_POST['productType']) && !empty($_POST['size'])) {
            $attr = $_POST['size']." MB";
            return $attr; 
        }
    }
    public function setAttributeType(){
        return "cd";
    }
}

class Book extends Products {

    public function getAttributeType(){
        if(!empty($_POST['sku']) && !empty($_POST['productName']) && !empty($_POST['productPrice']) && !empty($_POST['productType']) && !empty($_POST['weight'])) {
            $attr = $_POST['weight']." KG";
            return $attr; 
        }
    }
    public function setAttributeType(){
        return "book";
    }
}
