
$(document).ready(function(){
    $('.items').hide();
    $('.item-1').show();
    $("#productType").on("change", function(){
        var selectedType = $("#productType :selected").text();
        $("#selectType-db").val(selectedType);

        $('.items').hide();
        $('.item'+$(this).val()).show();
    }).val("-1");
});
