<?php
include 'dbc.php';
include 'products.php';
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- main css -->
    <link rel="stylesheet" type="text/css" media="screen" href="assets/main.css" />
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

</head>
<body>
    <!-- NAVIGATION -->
    <nav class="navbar navbar-expand navbar-dark bg-dark">
        <a class="navbar-brand" href="/">Products</a>
        <a class="navbar-brand" href="/add.php">Add products</a>
    </nav>
    <!-- end of NAVIGATION -->
    <!-- Product "show" and "delete" section-->
    <form name="delete" action="" method="POST">
        <div class="container">
            <div class ="row">
                        <?php  $dats = new Furniture();
                               $rows = $dats->getProducts();
                               $attr = $dats->getAttributeType();
                               if(!empty($rows)) {
                                   foreach ($rows as $row) { ?>
                                   <div class="col-12 col-lg-3 mx-auto card text-center product-card">
                                        <input type="checkbox" value="<?php echo $row['id'] ?>" name="checkbox" class="delete">
                                        <h3>
                                            <?php echo $row['SKU']; ?>
                                        </h3>
                                        <h5>
                                            <?php echo $row['Name']; ?>
                                        </h5>
                                        <?php echo $row['Price']; ?>&#36;
                                        <br>
                                        <?php echo $row['Atribute']; ?>
                                    </div>
                                <?php
                                   }
                               }
                               $dats = new Cd();
                               $rows = $dats->getProducts();
                               $attr = $dats->getAttributeType();
                               if(!empty($rows)) {
                                   foreach ($rows as $row) { ?>
                                   <div class="col-12 col-lg-3 mx-auto card text-center product-card">
                                        <input type="checkbox" value="<?php echo $row['id'] ?>" name="checkbox" class="delete">
                                        <h3>
                                            <?php echo $row['SKU']; ?>
                                        </h3>
                                        <h5>
                                            <?php echo $row['Name']; ?>
                                        </h5>
                                        <?php echo $row['Price']; ?>&#36;
                                        <br>
                                        <?php echo $row['Atribute']; ?>
                                    </div>
                                <?php
                                   }
                               }
                        
                               $dats = new Book();
                               $rows = $dats->getProducts();
                               $attr = $dats->getAttributeType();
                               if(!empty($rows)) {
                                   foreach ($rows as $row) { ?>
                                   <div class="col-12 col-lg-3 mx-auto card text-center product-card">
                                    <input type="checkbox" value="<?php echo $row['id'] ?>" name="checkbox" class="delete">
                                        <h3>
                                            <?php echo $row['SKU']; ?>
                                        </h3>
                                        <h5>
                                            <?php echo $row['Name']; ?>
                                        </h5>
                                        <?php echo $row['Price']; ?>&#36;
                                        <br>
                                        <?php echo $row['Atribute']; ?>
                                    </div>
                                <?php
                                   }
                               }
                        ?>
            </div>
                <div class="col-12 mt-3">
                    <button class="col-12 btn btn-danger btn-lg btn-block" type="submit" name="delete-items">Bulk delete</button>
                    
                </div>
            </div>
        </div>
    </form>

    <!-- end of product "show" and "delete" section-->
    <!-- bootsrap jQuery js -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <!-- bootstrap js -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <!-- bootsrap popper js -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
</body>
</html>
